package main

func fillMap(sl []int16) map[int16]bool {
	mapVals := map[int16]bool{}

	for _, v := range sl {
		mapVals[v] = true
	}

	return mapVals
}
func getUniqueFromMapAndSlice(mapVals map[int16]bool, sl []int16) []int16 {
	res := make([]int16, 0)

	for _, v := range sl {
		if mapVals[v] {
			res = append(res, v)
		}
	}

	return res
}

func StupidJoin(a, b []int16) []int16 {
	var unique []int16
	if len(a) < len(b) {
		unique = getUniqueFromMapAndSlice(fillMap(a), b)
	} else {
		unique = getUniqueFromMapAndSlice(fillMap(b), a)
	}

	return unique
}


func main() {

}